
module.exports = {
  'Help Application Validations': function (client) {

    client.url('http://localhost:3000')
      .windowMaximize()
      .waitForElementVisible('div.collapsible-all', 30000);
    client.expect.element('div.collapsible-all').text.to.contain('Collapse all');
    client.expect.element('div.contact-info p a').text.to.contain('service@RiskandSafetySolutions.com');
    client.expect.element('div.contact-info p:nth-child(3)').text.to.contain('phone 530-638-DESK (3375)');
    client.expect.element('div.help-page-left-section ul li:nth-child(1) a').text.to.contain('Tutorials')
    client.assert.attributeEquals("div.help-page-left-section ul li:nth-child(1) a", "class", "active")
    client.expect.element('div.help-page-left-section ul li:nth-child(2) a').text.to.contain('FAQs')
    client.click('div.help-page-left-section ul li:nth-child(2)')
      .waitForElementVisible('div.collapsible-all', 30000);
    client.click('div.collapsible-all')
    client.expect.element('div.help-page-right-section div div div div div').to.have.attribute('aria-expanded')
      .equals('false').before(3000);
    client.click('div.collapsible-all')
    client.expect.element('div.help-page-right-section div div div div div').to.have.attribute('aria-expanded')
      .equals('true').before(3000);
    client.click('div.search-col form div')
      .waitForElementVisible('#menu- div', 30000);
    client.click('#menu- ul li:nth-child(6)')
      .waitForElementVisible('div.collapsible-all', 30000);
    client.expect.element('div.mat-expansion-panel-header-title').text.to.contain('I cannot find my chemical')
    client.click('div.action-btns button:nth-child(3)')
    .waitForElementVisible('#modal-title',4000)
    client.expect.element('#modal-title').text.to.contain('Thanks for providing your feedback!')
    client.setValue('textarea', 'Some Feedback text');
    client.click('div.modal-action-btns button:nth-child(2)')
    .waitForElementVisible('div.mat-expansion-panel-header-title',4000)
    client.setValue('div.input-field input','Principal Investigator')
    .waitForElementVisible('h5 strong',4000)
    client.expect.element('h5 strong').text.to.contain('CIS - FAQ');
    client.click('div.search-col form div')
    .waitForElementVisible('#menu- div', 30000);
    client.click('#menu- ul li.MuiButtonBase-root-44')
    .waitForElementVisible('div.collapsible-all', 30000);
    client.setValue('input[type=text]','search')
      .waitForElementVisible('h5 strong',4000)
    client.expect.element('h5 strong').text.to.contain('Search Results for "search"')
    client.click('div.help-page-left-section ul li:nth-child(1) a')
      .waitForElementVisible('h5 strong',3000)
    client.expect.element('h5 strong').text.to.contain('Search Results for "search"')
    
    client.end();
  }
}
