'use strict';

// dependencies

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var concat = require('gulp-concat');

var SCSS_SRC = './src/assets/scss/**/*.scss';
var CSS_SRC = './src/assets/css/**/*.css';
var SCSS_DEST = './src/assets/css';
var CSS_DEST = './src/assets/mincss';

// create css task
gulp.task('css', function(){
    gulp.src(CSS_SRC)
        .pipe(minifyCSS())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(CSS_DEST))
});

//compile scss
gulp.task('compile_scss', function() {
    gulp.src(SCSS_SRC)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(changed(SCSS_DEST))
    .pipe(gulp.dest(SCSS_DEST));
});

//detect changes in scss
gulp.task('watch_scss', function() {
    gulp.watch(SCSS_SRC, ['compile_scss', 'css']);
});

//run tasks
gulp.task('default', ['watch_scss', 'css']);