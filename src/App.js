import React, { Component } from 'react';
import './assets/mincss/style.min.css';
import {  BrowserRouter } from 'react-router-dom';
import Header from './components/header/Header';
import { routes, RouteWithSubRoutes } from './Routes';
const basehref = process.env.REACT_APP_BASE_HREF || '/';
class App extends Component {
  render() {
    return (
      <BrowserRouter basehref={basehref}>
        <div>
        <Header />
        {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
