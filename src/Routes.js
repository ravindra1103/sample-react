
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SampleOverView from './components/SampleOverView';
import SampleOverView2 from './components/SampleOverView2';

export const routes = [
    {
      path: "/sample",
      component: SampleOverView
    },
    {
      path: "/sample2/:item",
      component: SampleOverView2
    },
    {
        path: "/sample2",
        component: SampleOverView2
    }
    
];

export const RouteWithSubRoutes = route => (
  <Switch>
    <Route
      exact path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  </Switch>
);
