import 'rxjs/Rx';
import {
    getHelpFeedAPI,
    getFaqFeedbyCodeAPI,
    getTutorialFeedByCodeAPI,
    filterSearch,
    filterByCategoryAndSearch
} from '../api/helpfeed';

import {
    getBrandingAPI
} from '../api/branding';
import * as ApplicationConstants from '../utils/ApplicationConstants';
import store from '../store';
import { convertCodetoSymbol, parseAndSortRssFeed } from '../utils/parse';
import { isValidToken } from '../utils/token';

export const GET_HELPFEED_INIT = 'GET_HELPFEED_INIT';
export const GET_HELPFEED_SUCCESS = 'GET_HELPFEED_SUCCESS';
export const GET_HELPFEED_FAILED = 'GET_HELPFEED_FAILED';
export const GET_DATA_BY_SELECTED_MENU_INIT = 'GET_DATA_BY_SELECTED_MENU_INIT';
export const GET_DATA_BY_SELECTED_MENU_SUCCESS = 'GET_DATA_BY_SELECTED_MENU_SUCCESS';
export const GET_DATA_BY_SELECTED_MENU_FAILED = 'GET_DATA_BY_SELECTED_MENU_FAILED';
export const GET_DATA_BY_SELECTED_MENU_AND_SEARCH_INIT = 'GET_DATA_BY_SELECTED_MENU_AND_SEARCH_INIT';
export const GET_DATA_BY_SELECTED_MENU_AND_SEARCH_SUCCESS = 'GET_DATA_BY_SELECTED_MENU_AND_SEARCH_SUCCESS';
export const GET_DATA_BY_SELECTED_MENU_AND_SEARCH_FAILED = 'GET_DATA_BY_SELECTED_MENU_AND_SEARCH_FAILED';
export const GET_DATA_BY_SELECTED_CATEGORY = 'GET_DATA_BY_SELECTED_CATEGORY';
export const GET_HELPFEED_CATEGORY_INIT = 'GET_HELPFEED_CATEGORY_INIT';
export const GET_HELPFEED_CATEGORY_SUCCESS = 'GET_HELPFEED_CATEGORY_SUCCESS';
export const GET_HELPFEED_CATEGORY_FAILED = 'GET_HELPFEED_CATEGORY_FAILED';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_INIT = 'GET_HELPFEED_CATEGORY_AND_SEARCH_INIT';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_SUCCESS = 'GET_HELPFEED_CATEGORY_AND_SEARCH_SUCCESS';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_FAILED = 'GET_HELPFEED_CATEGORY_AND_SEARCH_FAILED';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_INIT = 'GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_INIT';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_SUCCESS = 'GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_SUCCESS';
export const GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_FAILED = 'GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_FAILED';
export const GET_HELPFEED_SEARCH_INIT = 'GET_HELPFEED_SEARCH_INIT';
export const GET_HELPFEED_SEARCH_SUCCESS = 'GET_HELPFEED_SEARCH_SUCCESS';
export const GET_HELPFEED_SEARCH_FAILED = 'GET_HELPFEED_SEARCH_FAILED';
export const GET_HELPFEED_SEARCH_CANCELLED = 'GET_HELPFEED_SEARCH_CANCELLED';
export const GET_HELPFEED_SEARCH_CACHED_DATA_INIT = 'GET_HELPFEED_SEARCH_CACHED_DATA_INIT';
export const GET_HELPFEED_SEARCH_CACHED_DATA = 'GET_HELPFEED_SEARCH_CACHED_DATA';
export const CLEAR_SEARCHTERM_INIT = 'CLEAR_SEARCHTERM_INIT';
export const CLEAR_SEARCHTERM_SUCCESS = 'CLEAR_SEARCHTERM_SUCCESS';
export const CLEAR_SEARCHTERM_FAILED = 'CLEAR_SEARCHTERM_FAILED';
export const CHANGE_SELECTED_MENU = 'CHANGE_SELECTED_MENU';
export const UPDATE_CATEGORIES = 'UPDATE_CATEGORIES';
export const GET_BRANDING_DETAILS_INIT = 'GET_BRANDING_DETAILS_INIT';
export const GET_BRANDING_DETAILS_SUCCESS = 'GET_BRANDING_DETAILS_SUCCESS';
export const GET_BRANDING_DETAILS_FAILED = 'GET_BRANDING_DETAILS_FAILED';

export function getHelpFeed(token, props) {
    if (token && isValidToken(token)) {
        return function (dispatch) {
            dispatch({ type: GET_BRANDING_DETAILS_INIT, token });
            getBrandingAPI(token)
                .then(response => dispatch({ type: GET_BRANDING_DETAILS_SUCCESS, response }))
                .then(() => {
                    let categories = store.getState().helpfeed.brandingDetails.appList.map(item => item.shortName);
                    if (categories.find(item => item.toLowerCase() !== ApplicationConstants.PROFILE_CATEGORY_NAME.toLowerCase()))
                        categories.push(ApplicationConstants.PROFILE_CATEGORY_NAME);
                    categories.sort();
                    return dispatch({ type: UPDATE_CATEGORIES, categories });
                })
                .then(() => {
                    let apiToCall, selectedMenu = store.getState().helpfeed.selectedMenu, selectedCategory = store.getState().helpfeed.selectedCategory;
                    if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
                        apiToCall = getTutorialFeedByCodeAPI;
                    } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
                        apiToCall = getFaqFeedbyCodeAPI;
                    }
                    if (selectedCategory === ApplicationConstants.ALL_CATEGORY) {
                        apiToCall = getHelpFeedAPI;
                    }
                    dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
                    dispatch({ type: GET_HELPFEED_CATEGORY_INIT });
                    if (props && !props.category) {
                        apiToCall(selectedCategory)
                            .then(response => dispatch({ type: GET_HELPFEED_CATEGORY_SUCCESS, response }))
                            .catch(error => dispatch({ type: GET_HELPFEED_CATEGORY_FAILED, error }));
                    }
                })
                .catch(error => dispatch({ type: GET_BRANDING_DETAILS_FAILED, error }));
        };
    } else {
        return function (dispatch) {
            dispatch({ type: GET_HELPFEED_INIT });
            getHelpFeedAPI()
                .then(response => dispatch({ type: GET_HELPFEED_SUCCESS, response }))
                .then(() => {
                    let categories = Object.keys(store.getState().helpfeed.helpFeed);
                    if (categories && categories.length > 0) {
                        categories = [ApplicationConstants.ALL_CATEGORY, ...categories];
                    }
                    return dispatch({ type: UPDATE_CATEGORIES, categories })
                })
                .catch(error => dispatch({ type: GET_HELPFEED_FAILED, error }));
        };
    }
}

export function getHelpFeedBySelectedMenu(selectedMenu) {
    return function (dispatch) {
        dispatch({ type: GET_DATA_BY_SELECTED_MENU_INIT });
        filterSearch(store.getState().helpfeed.rawData, selectedMenu)
            .then(response => dispatch({ type: GET_DATA_BY_SELECTED_MENU_SUCCESS, response }))
            .then(() => {
                let categories = [];
                if (store.getState().helpfeed.brandingDetails) {
                    categories = store.getState().helpfeed.brandingDetails.appList.map(item => item.shortName);
                } else {
                    categories = Object.keys(store.getState().helpfeed.helpFeed);
                    if (categories && categories.length > 0) {
                        categories = [ApplicationConstants.ALL_CATEGORY, ...categories];
                    }
                }
                return dispatch({ type: UPDATE_CATEGORIES, categories })
            })
            .catch(error => dispatch({ type: GET_DATA_BY_SELECTED_MENU_FAILED, error }));
    };
}

export function getHelpFeedBySelectedMenuAndSearch(selectedMenu, searchTerm) {
    return function (dispatch) {
        dispatch({ type: GET_DATA_BY_SELECTED_MENU_AND_SEARCH_INIT });
        filterByCategoryAndSearch(store.getState().helpfeed.rawData, selectedMenu, searchTerm)
            .then(response => dispatch({ type: GET_DATA_BY_SELECTED_MENU_AND_SEARCH_SUCCESS, response }))
            .catch(error => dispatch({ type: GET_DATA_BY_SELECTED_MENU_AND_SEARCH_FAILED, error }));
    };
}

export function changeSelectedMenu(selectedMenu) {
    return function (dispatch) {
        dispatch({ type: CHANGE_SELECTED_MENU, selectedMenu });
    };
}

export function getHelpFeedBySelectedCategory(selectedMenu, selectedCategory) {
    let apiToCall;
    if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
        apiToCall = getTutorialFeedByCodeAPI;
    } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
        apiToCall = getFaqFeedbyCodeAPI;
    }
    if (selectedCategory === ApplicationConstants.ALL_CATEGORY) {
        apiToCall = getHelpFeedAPI;
    }
    return function (dispatch) {
        dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
        dispatch({ type: GET_HELPFEED_CATEGORY_INIT });
        apiToCall(selectedCategory)
            .then(response => dispatch({ type: GET_HELPFEED_CATEGORY_SUCCESS, response }))
            .catch(error => dispatch({ type: GET_HELPFEED_CATEGORY_FAILED, error }));
    }
}

export function getHelpFeedByNewCategory(selectedMenu, selectedCategory) {
    let apiToCall;
    return function (dispatch) {
        apiToCall = getTutorialFeedByCodeAPI;
        apiToCall(selectedCategory)
            .then(response => {
                let resLen = parseAndSortRssFeed(response, selectedMenu);
                if (resLen.categories.length <= 0) {
                    apiToCall = getFaqFeedbyCodeAPI;
                    apiToCall(selectedCategory)
                        .then(response => {
                            let resFaqLen = parseAndSortRssFeed(response, selectedMenu);
                            if (resFaqLen.categories.length <= 0) {
                                let categories = store.getState().helpfeed.categories;
                                dispatch(getHelpFeedBySelectedCategory(selectedMenu, categories[0]))
                            }
                            else {
                                store.getState().helpfeed.categories.push(resFaqLen.categories[0]);
                                let selectedCategory = resFaqLen.categories[0]
                                dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
                                dispatch({ type: GET_HELPFEED_CATEGORY_SUCCESS, response })
                            }
                        })
                }
                else {
                    store.getState().helpfeed.categories.push(resLen.categories[0]);
                    let selectedCategory = resLen.categories[0]
                    dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
                    dispatch({ type: GET_HELPFEED_CATEGORY_SUCCESS, response })
                }
            })
            .catch(error => dispatch({ type: GET_HELPFEED_CATEGORY_FAILED, error }));
    }
}

export function getHelpFeedBySelectedCategoryAndSearch(selectedMenu, selectedCategory) {
    let apiToCall;
    if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
        apiToCall = getTutorialFeedByCodeAPI;
    } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
        apiToCall = getFaqFeedbyCodeAPI;
    }
    if (selectedCategory === ApplicationConstants.ALL_CATEGORY) {
        apiToCall = getHelpFeedAPI;
    }
    return function (dispatch) {
        dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
        dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_INIT });
        apiToCall(selectedCategory)
            .then(response => dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_SUCCESS, response }))
            .catch(error => dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_FAILED, error }));
    };
}

export function getHelpFeedBySelectedCategoryAndClearSearch(selectedMenu, selectedCategory) {
    let apiToCall;
    if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
        apiToCall = getTutorialFeedByCodeAPI;
    } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
        apiToCall = getFaqFeedbyCodeAPI;
    }
    if (selectedCategory === ApplicationConstants.ALL_CATEGORY) {
        apiToCall = getHelpFeedAPI;
    }
    return function (dispatch) {
        dispatch({ type: GET_DATA_BY_SELECTED_CATEGORY, selectedCategory });
        dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_INIT });
        apiToCall(selectedCategory)
            .then(response => dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_SUCCESS, response }))
            .catch(error => dispatch({ type: GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_FAILED, error }));
    };
}

export function getHelpFeedSearch(searchTerm) {
    searchTerm = convertCodetoSymbol(searchTerm);
    return {
        type: GET_HELPFEED_SEARCH_INIT, searchTerm
    };
}

export function getHelpFeedSearchSuccess(response) {
    response = response ? response.response : {};
    return {
        type: GET_HELPFEED_SEARCH_SUCCESS,
        response
    };
}

export function getHelpFeedSearchCancel() {
    return {
        type: GET_HELPFEED_SEARCH_CANCELLED
    };
}


export function getHelpFeedSearchCached(searchTerm) {
    return function (dispatch) {
        dispatch({ type: GET_HELPFEED_SEARCH_CACHED_DATA_INIT });
        dispatch({ type: GET_HELPFEED_SEARCH_CACHED_DATA, searchTerm });
    };
}

export function clearSearchTerm() {
    return function (dispatch) {
        dispatch({ type: CLEAR_SEARCHTERM_INIT });
        filterSearch(store.getState().helpfeed.rawData, store.getState().helpfeed.selectedMenu)
            .then(response => dispatch({ type: CLEAR_SEARCHTERM_SUCCESS, response }))
            .catch(error => dispatch({ type: CLEAR_SEARCHTERM_FAILED, error }));
    };
}