import {
  postActivityAPI,
  postFeedbackAPI,
  postSearchAPI,
  updateActivityAPI,
  updateSearchAPI
} from '../api/helptracking';

import store from '../store';

export const POST_ACTIVITY_CATEGORY_INIT = 'POST_ACTIVITY_CATEGORY_INIT';
export const POST_ACTIVITY_CATEGORY_SUCCESS = 'POST_ACTIVITY_CATEGORY_SUCCESS';
export const POST_ACTIVITY_CATEGORY_FAILED = 'POST_ACTIVITY_CATEGORY_FAILED';
export const POST_ACTIVITY_TYPE_INIT = 'POST_ACTIVITY_TYPE_INIT';
export const POST_ACTIVITY_TYPE_SUCCESS = 'POST_ACTIVITY_TYPE_SUCCESS';
export const POST_ACTIVITY_TYPE_FAILED = 'POST_ACTIVITY_TYPE_FAILED';
export const POST_FEEDBACK_INIT = 'POST_FEEDBACK_INIT';
export const POST_FEEDBACK_SUCCESS = 'POST_FEEDBACK_SUCCESS';
export const POST_FEEDBACK_FAILED = 'POST_FEEDBACK_FAILED';
export const POST_UPDATE_ACTIVITY_INIT = 'POST_UPDATE_ACTIVITY_INIT';
export const POST_UPDATE_ACTIVITY_SUCCESS = 'POST_UPDATE_ACTIVITY_SUCCESS';
export const POST_UPDATE_ACTIVITY_FAILED = 'POST_UPDATE_ACTIVITY_FAILED';
export const POST_UPDATE_SEARCH_INIT = 'POST_UPDATE_SEARCH_INIT';
export const POST_UPDATE_SEARCH_SUCCESS = 'POST_UPDATE_SEARCH_SUCCESS';
export const POST_UPDATE_SEARCH_FAILED = 'POST_UPDATE_SEARCH_FAILED';
export const POST_UPDATE_EMAIL_INIT = 'POST_UPDATE_EMAIL_INIT';
export const POST_UPDATE_EMAIL_SUCCESS = 'POST_UPDATE_EMAIL_SUCCESS';
export const POST_UPDATE_EMAIL_FAILED = 'POST_UPDATE_EMAIL_FAILED';
export const POST_UPDATE_PHONE_INIT = 'POST_UPDATE_PHONE_INIT';
export const POST_UPDATE_PHONE_SUCCESS = 'POST_UPDATE_PHONE_SUCCESS';
export const POST_UPDATE_PHONE_FAILED = 'POST_UPDATE_PHONE_FAILED';
export const POST_UPDATE_SEARCH_TUTORIAL_INIT = 'POST_UPDATE_SEARCH_TUTORIAL_INIT';
export const POST_UPDATE_SEARCH_TUTORIAL_SUCCESS = 'POST_UPDATE_SEARCH_TUTORIAL_SUCCESS';
export const POST_UPDATE_SEARCH_TUTORIAL_FAILED = 'POST_UPDATE_SEARCH_TUTORIAL_FAILED';
export const POST_UPDATE_SEARCH_FAQ_INIT = 'POST_UPDATE_SEARCH_FAQ_INIT';
export const POST_UPDATE_SEARCH_FAQ_SUCCESS = 'POST_UPDATE_SEARCH_FAQ_SUCCESS';
export const POST_UPDATE_SEARCH_FAQ_FAILED = 'POST_UPDATE_SEARCH_FAQ_FAILED';
export const UPDATE_FEEDBACK_RECEIVED = 'UPDATE_FEEDBACK_RECEIVED';

export function postActivityCategory(activityData) {
  return function (dispatch) {
    dispatch({ type: POST_ACTIVITY_CATEGORY_INIT });
    postActivityAPI(activityData, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_ACTIVITY_CATEGORY_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_ACTIVITY_CATEGORY_FAILED, error }));
  };
}

export function postActivityType(activityData) {
  return function (dispatch) {
    dispatch({ type: POST_ACTIVITY_TYPE_INIT });
    postActivityAPI(activityData, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_ACTIVITY_TYPE_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_ACTIVITY_TYPE_FAILED, error }));
  };
}

export function postFeedback(feedbackData, category) {
  return function (dispatch) {
    dispatch({ type: UPDATE_FEEDBACK_RECEIVED, response: { category, guid: feedbackData.articleId } });
    dispatch({ type: POST_FEEDBACK_INIT });
    postFeedbackAPI(feedbackData, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_FEEDBACK_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_FEEDBACK_FAILED, error }));
  };
}

export function postActivitySearchTutorial(searchData, feed) {
  let searchResults = 0;
  for (let element in feed) {
    searchResults = searchResults + feed[element].length;
  }

  return function (dispatch) {
    dispatch({ type: POST_UPDATE_SEARCH_TUTORIAL_INIT });
    postSearchAPI({ ...searchData, searchResults }, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_SEARCH_TUTORIAL_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_SEARCH_TUTORIAL_FAILED, error }));
  };
}

export function postActivitySearchFaq(searchData, feed) {
  let searchResults = 0;
  for (let element in feed) {
    searchResults = searchResults + feed[element].length;
  }

  return function (dispatch) {
    dispatch({ type: POST_UPDATE_SEARCH_FAQ_INIT });
    postSearchAPI({ ...searchData, searchResults }, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_SEARCH_FAQ_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_SEARCH_FAQ_FAILED, error }));
  };
}

export function updateActivity(activityId) {
  return function (dispatch) {
    dispatch({ type: POST_UPDATE_ACTIVITY_INIT });
    updateActivityAPI(activityId, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_ACTIVITY_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_ACTIVITY_FAILED, error }));
  };
}

export function updateSearch(searchId) {
  return function (dispatch) {
    dispatch({ type: POST_UPDATE_SEARCH_INIT });
    updateSearchAPI(searchId, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_SEARCH_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_SEARCH_FAILED, error }));
  };
}

export function postActivityEmail(activityData) {
  return function (dispatch) {
    dispatch({ type: POST_UPDATE_EMAIL_INIT });
    postActivityAPI(activityData, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_EMAIL_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_EMAIL_FAILED, error }));
  };
}

export function postActivityPhone(activityData) {
  return function (dispatch) {
    dispatch({ type: POST_UPDATE_PHONE_INIT });
    postActivityAPI(activityData, store.getState().helpfeed.tokenDetails)
      .then(response => dispatch({ type: POST_UPDATE_PHONE_SUCCESS, response }))
      .catch(error => dispatch({ type: POST_UPDATE_PHONE_FAILED, error }));
  };
}
