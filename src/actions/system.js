import { getIpAddressAPI } from '../api/system';

export const GET_IPADDRESS_INIT = 'GET_IPADDRESS_INIT';
export const GET_IPADDRESS_SUCCESS = 'GET_IPADDRESS_SUCCESS';
export const GET_IPADDRESS_FAILED = 'GET_IPADDRESS_FAILED';

export function getIPAddress() {
    return function (dispatch) {
        dispatch({ type: GET_IPADDRESS_INIT });
        getIpAddressAPI()
            .then(response => dispatch({ type: GET_IPADDRESS_SUCCESS, response }))
            .catch(error => dispatch({ type: GET_IPADDRESS_FAILED, error }));
    };
}