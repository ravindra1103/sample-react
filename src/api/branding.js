export function getBrandingAPI(authHeader) {
    let myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + authHeader);
    let myInit = { method: 'GET', headers: myHeaders };
    let myRequest = new Request(`${process.env.REACT_APP_BRANDING_URL}/api/branding?environment=${process.env.REACT_APP_ENV}`, myInit);
    return fetch(myRequest).then(res => res.json());
}