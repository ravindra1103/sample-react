import { parseAndSortRssFeed, searchItemsInHelpFeed } from '../utils/parse';

export function getHelpFeedAPI() {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/getFeed`).then(res => res.text());
}

export function getFaqFeedbyCodeAPI(faqCode) {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/getFaqFeedbyCode?name=${faqCode}`).then(res => res.text());
}

export function getTutorialFeedByCodeAPI(tutorialCode) {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/getTutorialFeedbyCode?name=${tutorialCode}`).then(res => res.text());
}

export function getFeedSearchAPI(searchTerm) {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/getFeedbysearch?name=${searchTerm}`).then(res => res.text());
}

export function filterSearch(rawData, selectedMenu) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(
                parseAndSortRssFeed(rawData, selectedMenu).data
            );
        }, 1000);
    });
}

export function filterByCategoryAndSearch(rawData, selectedMenu, searchTerm) {
    let helpFeed = parseAndSortRssFeed(rawData, selectedMenu).data;
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(
                searchItemsInHelpFeed(helpFeed, searchTerm)
            );
        }, 1000);
    });
}