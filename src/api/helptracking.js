export function postActivityAPI(activityData, tokenDetails = {}) {
        let data = {
                "ipAddress": activityData.ipAddress,
                "userAgent": activityData.userAgent,
                "userId": tokenDetails.sub,
                "email": tokenDetails.email,
                "name": tokenDetails.firstName ? `${tokenDetails.firstName} ${tokenDetails.lastName}`: undefined,
                "applicationCode": `rss.${activityData.applicationCode.toLowerCase()}`,
                "createdDate": Date.now(),
                "activity": activityData.activity,
                "startDate": Date.now(),
                "endDate": Date.now()
        };
        return fetch(`${process.env.REACT_APP_SERVER_URL}/activity`, {
                body: JSON.stringify(data),
                headers: {
                        'content-type': 'application/json'
                },
                method: 'POST'
        }).then(res => res.json());
}

export function postFeedbackAPI(feedbackData, tokenDetails = {}) {
        let data = {
                "ipAddress": feedbackData.ipAddress,
                "userAgent": feedbackData.userAgent,
                "articleId": feedbackData.articleId,
                "comments": feedbackData.comments,
                "userId": tokenDetails.sub,
                "email": tokenDetails.email,
                "name": tokenDetails.firstName ? `${tokenDetails.firstName} ${tokenDetails.lastName}`: undefined,
                "isHelpful": feedbackData.isHelpful,
                "applicationCode": `rss.${feedbackData.applicationCode.toLowerCase()}`,
                "createdDate": Date.now(),
                "startDate": Date.now(),
                "endDate": Date.now()
        };
        return fetch(`${process.env.REACT_APP_SERVER_URL}/feedback`, {
                body: JSON.stringify(data),
                headers: {
                        'content-type': 'application/json'
                },
                method: 'POST'
        }).then(res => res.json());
}

export function postSearchAPI(searchData, tokenDetails = {}) {
        let data = {
                "ipAddress": searchData.ipAddress,
                "userAgent": searchData.userAgent,
                "userId": tokenDetails.sub,
                "email": tokenDetails.email,
                "name": tokenDetails.firstName ? `${tokenDetails.firstName} ${tokenDetails.lastName}`: undefined,
                "applicationCode": `rss.${searchData.applicationCode.toLowerCase()}`,
                "searchText": searchData.searchText,
                "searchResults": searchData.searchResults,
                "searchType": searchData.searchType
        };
        return fetch(`${process.env.REACT_APP_SERVER_URL}/search`, {
                body: JSON.stringify(data),
                headers: {
                        'content-type': 'application/json'
                },
                method: 'POST'
        }).then(res => res.json());
}

export function updateActivityAPI(activityId) {
        let data = {
                endDate: Date.now(),
                id: activityId
        };
        return fetch(`${process.env.REACT_APP_SERVER_URL}/activity/update`, {
                body: JSON.stringify(data),
                headers: {
                        'content-type': 'application/json'
                },
                method: 'POST'
        }).then(res => res.json());
}

export function updateSearchAPI(searchId) {
        let data = {
                endDate: Date.now(),
                id: searchId
        };
        return fetch(`${process.env.REACT_APP_SERVER_URL}/search/update`, {
                body: JSON.stringify(data),
                headers: {
                        'content-type': 'application/json'
                },
                method: 'POST'
        }).then(res => res.json());
}