export function getIpAddressAPI() {
    return fetch(`https://api.ipify.org?format=json`).then(res => res.json());
}