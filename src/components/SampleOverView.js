import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getIPAddress } from '../actions/Actions';

export class SampleOverView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisplay: false
        };
    }
    componentDidMount() {
        this.props.dispatch(getIPAddress());
    }
    render() {
        console.log("this.props", this.props);
        return (
            <h1>
                Sample Overview
                {this.props.ipAddress || ''}
            </h1>
        )
    }
}
SampleOverView.propTypes = {
    ipAddress: PropTypes.string
};

let select = (state, props) => ({
    ipAddress: state.system.ipAddress
});

export default connect(select)(SampleOverView);
