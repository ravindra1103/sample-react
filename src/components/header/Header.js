import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';

export class Header extends Component {
  
  changeFavicon = (src) => {
    let link = document.createElement('link');
    link.rel = 'shortcut icon';
    link.href = src;
    document.head.appendChild(link);
  }

  render() {
    return (
      <div>
        <AppBar className='mat-toolbar-layout' position="static">
          <Toolbar className='help-toolbar'>
            <IconButton color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit">
              <span className="riskTitle">Sample Application</span><span className="projectTitle">Sample</span>
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export  default Header;