import 'rxjs';
import { ajax } from 'rxjs/observable/dom/ajax';
import { GET_HELPFEED_SEARCH_INIT, GET_HELPFEED_SEARCH_CANCELLED } from '../actions/Actions';
import { getHelpFeedSearchSuccess } from '../actions/Actions';

export const getHelpFeedSearchEpic = action$ =>
    action$.ofType(GET_HELPFEED_SEARCH_INIT)
        .mergeMap(action =>
            ajax({
                url: `${process.env.REACT_APP_SERVER_URL}/getFeedbysearch?name=${action.searchTerm}`,
                method: 'GET',
                responseType: 'text'
            })
                .map(response => getHelpFeedSearchSuccess(response))
                .takeUntil(action$.ofType(GET_HELPFEED_SEARCH_CANCELLED))
        );