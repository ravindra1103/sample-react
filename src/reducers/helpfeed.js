import { parseAndSortRssFeed, parseResultsAfterFeedBack, searchItemsInCategory } from '../utils/parse';
import { decodeToken } from '../utils/token';
import {
  GET_HELPFEED_INIT,
  GET_HELPFEED_SUCCESS,
  GET_HELPFEED_FAILED,
  GET_DATA_BY_SELECTED_MENU_INIT,
  GET_DATA_BY_SELECTED_MENU_SUCCESS,
  GET_DATA_BY_SELECTED_MENU_FAILED,
  GET_DATA_BY_SELECTED_MENU_AND_SEARCH_INIT,
  GET_DATA_BY_SELECTED_MENU_AND_SEARCH_SUCCESS,
  GET_DATA_BY_SELECTED_MENU_AND_SEARCH_FAILED,
  CHANGE_SELECTED_MENU,
  GET_DATA_BY_SELECTED_CATEGORY,
  GET_HELPFEED_CATEGORY_INIT,
  GET_HELPFEED_CATEGORY_SUCCESS,
  GET_HELPFEED_CATEGORY_FAILED,
  GET_HELPFEED_CATEGORY_AND_SEARCH_INIT,
  GET_HELPFEED_CATEGORY_AND_SEARCH_SUCCESS,
  GET_HELPFEED_CATEGORY_AND_SEARCH_FAILED,
  GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_INIT,
  GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_SUCCESS,
  GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_FAILED,
  GET_HELPFEED_SEARCH_INIT,
  GET_HELPFEED_SEARCH_SUCCESS,
  GET_HELPFEED_SEARCH_FAILED,
  GET_HELPFEED_SEARCH_CACHED_DATA_INIT,
  GET_HELPFEED_SEARCH_CACHED_DATA,
  CLEAR_SEARCHTERM_INIT,
  CLEAR_SEARCHTERM_SUCCESS,
  CLEAR_SEARCHTERM_FAILED,
  UPDATE_FEEDBACK_RECEIVED,
  UPDATE_CATEGORIES,
  GET_BRANDING_DETAILS_INIT,
  GET_BRANDING_DETAILS_SUCCESS,
  GET_BRANDING_DETAILS_FAILED,
  GET_HELPFEED_SEARCH_CANCELLED
} from '../actions/Actions';

import * as ApplicationConstants from '../utils/ApplicationConstants';

export const initialState = {
  helpFeed: undefined,
  helpCategoryFeed: undefined,
  HELP_CONTENT_TYPE: [ApplicationConstants.TUTORIAL_MENU_ITEM_NAME, ApplicationConstants.FAQS_MENU_ITEM_NAME],
  selectedMenu: ApplicationConstants.TUTORIAL_MENU_ITEM_NAME,
  selectedCategory: ApplicationConstants.ALL_CATEGORY,
  categoryResultFound: false,
  categories: [],
  rawData: undefined,
  searchTerm: undefined,
  searchStatus: 'Loading',
  helpLoadingStatus: 'Loading',
  brandingDetails: undefined,
  tokenDetails: undefined,
  error: undefined,
  currentSearch: ''
};

const handlers = {
  [GET_HELPFEED_INIT]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    categories: [],
    helpLoadingStatus: 'Loading',
    error: undefined
  }),
  [GET_HELPFEED_SUCCESS]: (state, action) => ({
    ...state,
    rawData: action.response,
    helpLoadingStatus: 'Completed',
    helpFeed: parseAndSortRssFeed(action.response, state.selectedMenu).data,
    error: undefined
  }),
  [GET_HELPFEED_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    categories: [],
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_BRANDING_DETAILS_INIT]: (state, action) => ({
    ...state,
    tokenDetails: decodeToken(action.token),
    brandingDetails: undefined
  }),
  [GET_BRANDING_DETAILS_SUCCESS]: (state, action) => ({
    ...state,
    brandingDetails: action.response
  }),
  [GET_BRANDING_DETAILS_FAILED]: (state, action) => ({
    ...state,
    brandingDetails: undefined
  }),
  [GET_DATA_BY_SELECTED_MENU_INIT]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    categories: [],
    helpLoadingStatus: 'Loading',
    error: undefined
  }),
  [GET_DATA_BY_SELECTED_MENU_SUCCESS]: (state, action) => ({
    ...state,
    helpLoadingStatus: 'Completed',
    helpFeed: action.response,
    error: undefined
  }),
  [GET_DATA_BY_SELECTED_MENU_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    categories: [],
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_DATA_BY_SELECTED_MENU_AND_SEARCH_INIT]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    searchStatus: 'Loading',
    helpLoadingStatus: 'Loading',
    error: undefined,
    currentSearch: state.searchTerm
  }),
  [GET_DATA_BY_SELECTED_MENU_AND_SEARCH_SUCCESS]: (state, action) => ({
    ...state,
    searchStatus: 'Completed',
    helpLoadingStatus: 'Completed',
    helpFeed: action.response,
    error: undefined
  }),
  [GET_DATA_BY_SELECTED_MENU_AND_SEARCH_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    searchStatus: 'Failed',
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_DATA_BY_SELECTED_CATEGORY]: (state, action) => ({
    ...state,
    selectedCategory: action.selectedCategory
  }),
  [CHANGE_SELECTED_MENU]: (state, action) => ({
    ...state,
    selectedMenu: action.selectedMenu
  }),
  [GET_HELPFEED_CATEGORY_INIT]: (state, action) => ({
    ...state,
    searchTerm: undefined,
    helpFeed: undefined,
    helpLoadingStatus: 'Loading',
    categoryResultFound: false,
    error: undefined
  }),
  [GET_HELPFEED_CATEGORY_SUCCESS]: (state, action) => {
    let { data } = parseAndSortRssFeed(action.response, state.selectedMenu);
    return {
      ...state,
      helpLoadingStatus: 'Completed',
      rawData: action.response,
      helpFeed: data,
      helpCategoryFeed: data,
      searchTerm: undefined,
      categoryResultFound: Object.keys(data || {}).length > 0,
      error: undefined
    }
  },
  [GET_HELPFEED_CATEGORY_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_INIT]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    helpLoadingStatus: 'Loading',
    categoryResultFound: false,
    searchTerm: undefined,
    error: undefined
  }),
  [GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_SUCCESS]: (state, action) => {
    let { data } =  parseAndSortRssFeed(action.response, state.selectedMenu, state.searchTerm);
    return (
      {
        ...state,
        helpLoadingStatus: 'Completed',
        rawData: action.response,
        helpFeed: data,
        helpCategoryFeed: data,
        categoryResultFound: Object.keys(data || {}).length > 0,
        error: undefined
      }
    );
  },
  [GET_HELPFEED_CATEGORY_AND_SEARCH_CLEAR_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_HELPFEED_CATEGORY_AND_SEARCH_INIT]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    helpLoadingStatus: 'Loading',
    categoryResultFound: false,
    error: undefined,
    currentSearch: state.searchTerm
  }),
  [GET_HELPFEED_CATEGORY_AND_SEARCH_SUCCESS]: (state, action) => {
    let { data } =  parseAndSortRssFeed(action.response, state.selectedMenu, state.searchTerm);
    let { data: categoryData, categories } =  parseAndSortRssFeed(action.response, state.selectedMenu);
    return (
      {
        ...state,
        helpLoadingStatus: 'Completed',
        rawData: action.response,
        helpFeed: data,
        helpCategoryFeed: categoryData,
        categoryResultFound: categories.length > 0,
        error: undefined
      }
    );
  },
  [GET_HELPFEED_CATEGORY_AND_SEARCH_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_HELPFEED_SEARCH_INIT]: (state, action) => ({
    ...state,
    searchStatus: 'Loading',
    searchTerm: action.searchTerm,
    helpLoadingStatus: 'Loading',
    error: undefined,
    currentSearch: action.searchTerm
  }),
  [GET_HELPFEED_SEARCH_SUCCESS]: (state, action) => ({
    ...state,
    searchStatus: 'Completed',
    helpLoadingStatus: 'Completed',
    helpFeed: parseAndSortRssFeed(action.response, state.selectedMenu, state.searchTerm).data,
    error: undefined
  }),
  [GET_HELPFEED_SEARCH_FAILED]: (state, action) => ({
    ...state,
    helpFeed: undefined,
    searchStatus: 'Completed',
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [GET_HELPFEED_SEARCH_CANCELLED]: (state, action) => ({
    ...state,
    searchStatus: 'Completed',
    helpLoadingStatus: 'Completed'
  }),
  [GET_HELPFEED_SEARCH_CACHED_DATA_INIT]: (state, action) => ({
    ...state,
    searchStatus: 'Loading',
    helpLoadingStatus: 'Loading',
    error: undefined,
    currentSearch: state.searchTerm
  }),
  [GET_HELPFEED_SEARCH_CACHED_DATA]: (state, action) => ({
    ...state,
    searchTerm: action.searchTerm,
    searchStatus: 'Completed',
    helpLoadingStatus: 'Completed',
    helpFeed: searchItemsInCategory(state.helpCategoryFeed, state.selectedCategory, action.searchTerm),
    error: undefined
  }),
  [CLEAR_SEARCHTERM_INIT]: (state, action) => ({
    ...state,
    helpLoadingStatus: 'Loading',
    error: undefined,
    currentSearch: ''
  }),
  [CLEAR_SEARCHTERM_SUCCESS]: (state, action) => {
    if(state.currentSearch === '') {
      return {
        ...state,
        helpLoadingStatus: 'Completed',
        helpFeed: action.response,
        searchTerm: undefined,
        error: undefined
      }
    }
    else {
      return {
        ...state,
        helpLoadingStatus: 'Completed',
        error: undefined
      }
    }
  },
  [CLEAR_SEARCHTERM_FAILED]: (state, action) => ({
    ...state,
    helpLoadingStatus: 'Failed',
    error: action.error
  }),
  [UPDATE_FEEDBACK_RECEIVED]: (state, action) => ({
    ...state,
    guid: action.response.guid,
    category: action.response.category,
    helpFeed: parseResultsAfterFeedBack(state.helpFeed, action.response.guid, action.response.category)
  }),
  [UPDATE_CATEGORIES]: (state, action) => ({
    ...state,
    categories: action.categories && action.categories.length > 0 ? action.categories.sort() : [],
    selectedCategory: action.categories && action.categories.length > 0 ? action.categories[0] : ApplicationConstants.ALL_CATEGORY
  })
};

export default function helpFeedReducer(state = initialState, action) {
  let handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}