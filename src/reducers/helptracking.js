import {
  POST_ACTIVITY_CATEGORY_INIT,
  POST_ACTIVITY_CATEGORY_SUCCESS,
  POST_ACTIVITY_CATEGORY_FAILED,
  POST_ACTIVITY_TYPE_INIT,
  POST_ACTIVITY_TYPE_SUCCESS,
  POST_ACTIVITY_TYPE_FAILED,
  POST_FEEDBACK_INIT,
  POST_FEEDBACK_SUCCESS,
  POST_FEEDBACK_FAILED,
  POST_UPDATE_ACTIVITY_INIT,
  POST_UPDATE_ACTIVITY_SUCCESS,
  POST_UPDATE_ACTIVITY_FAILED,
  POST_UPDATE_EMAIL_INIT,
  POST_UPDATE_EMAIL_SUCCESS,
  POST_UPDATE_EMAIL_FAILED,
  POST_UPDATE_PHONE_INIT,
  POST_UPDATE_PHONE_SUCCESS,
  POST_UPDATE_PHONE_FAILED,
  POST_UPDATE_SEARCH_FAQ_INIT,
  POST_UPDATE_SEARCH_FAQ_SUCCESS,
  POST_UPDATE_SEARCH_FAQ_FAILED,
  POST_UPDATE_SEARCH_TUTORIAL_INIT,
  POST_UPDATE_SEARCH_TUTORIAL_SUCCESS,
  POST_UPDATE_SEARCH_TUTORIAL_FAILED,
  POST_UPDATE_SEARCH_INIT,
  POST_UPDATE_SEARCH_SUCCESS,
  POST_UPDATE_SEARCH_FAILED
} from '../actions/Actions';

export const initialState = {
  postFeedBackDetails: undefined,
  postSearchDetails: undefined,
  postActivityCategoryDetails: undefined,
  postActivityTypeDetails: undefined,
  postActivityPhoneDetails: undefined,
  postActivityEmailDetails: undefined,
  postActivitySearchTutorialDetails: undefined,
  postActivitySearchFaqDetails: undefined,
  ipAddress: '',
  error: undefined
};

const handlers = {
  [POST_ACTIVITY_CATEGORY_INIT]: (state, action) => ({
    ...state,
    postActivityCategoryDetails: undefined,
    error: undefined
  }),
  [POST_ACTIVITY_CATEGORY_SUCCESS]: (state, action) => ({
    ...state,
    postActivityCategoryDetails: action.response,
    error: undefined
  }),
  [POST_ACTIVITY_CATEGORY_FAILED]: (state, action) => ({
    ...state,
    postActivityCategoryDetails: undefined,
    error: action.error
  }),
  [POST_ACTIVITY_TYPE_INIT]: (state, action) => ({
    ...state,
    postActivityTypeDetails: undefined,
    error: undefined
  }),
  [POST_ACTIVITY_TYPE_SUCCESS]: (state, action) => ({
    ...state,
    postActivityTypeDetails: action.response,
    error: undefined
  }),
  [POST_ACTIVITY_TYPE_FAILED]: (state, action) => ({
    ...state,
    postActivityTypeDetails: undefined,
    error: action.error
  }),
  [POST_FEEDBACK_INIT]: (state, action) => ({
    ...state,
    postFeedBackDetails: undefined,
    error: undefined
  }),
  [POST_FEEDBACK_SUCCESS]: (state, action) => ({
    ...state,
    postFeedBackDetails: action.response,
    error: undefined
  }),
  [POST_FEEDBACK_FAILED]: (state, action) => ({
    ...state,
    postFeedBackDetails: undefined,
    error: action.error
  }),
  [POST_UPDATE_ACTIVITY_INIT]: (state, action) => ({
    ...state,
    error: undefined
  }),
  [POST_UPDATE_ACTIVITY_SUCCESS]: (state, action) => ({
    ...state,
    error: undefined
  }),
  [POST_UPDATE_ACTIVITY_FAILED]: (state, action) => ({
    ...state,
    error: action.error
  }),
  [POST_UPDATE_SEARCH_INIT]: (state, action) => ({
    ...state,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_SUCCESS]: (state, action) => ({
    ...state,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_FAILED]: (state, action) => ({
    ...state,
    error: action.error
  }),
  [POST_UPDATE_SEARCH_FAQ_INIT]: (state, action) => ({
    ...state,
    postActivitySearchFaqDetails: undefined,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_FAQ_SUCCESS]: (state, action) => ({
    ...state,
    postActivitySearchFaqDetails: action.response,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_FAQ_FAILED]: (state, action) => ({
    ...state,
    postActivitySearchFaqDetails: undefined,
    error: action.error
  }),
  [POST_UPDATE_SEARCH_TUTORIAL_INIT]: (state, action) => ({
    ...state,
    postActivitySearchTutorialDetails: undefined,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_TUTORIAL_SUCCESS]: (state, action) => ({
    ...state,
    postActivitySearchTutorialDetails: action.response,
    error: undefined
  }),
  [POST_UPDATE_SEARCH_TUTORIAL_FAILED]: (state, action) => ({
    ...state,
    postActivitySearchTutorialDetails: undefined,
    error: action.error
  }),
  [POST_UPDATE_EMAIL_INIT]: (state, action) => ({
    ...state,
    postActivityEmailDetails: undefined,
    error: undefined
  }),
  [POST_UPDATE_EMAIL_SUCCESS]: (state, action) => ({
    ...state,
    postActivityEmailDetails: action.response,
    error: undefined
  }),
  [POST_UPDATE_EMAIL_FAILED]: (state, action) => ({
    ...state,
    postActivityEmailDetails: undefined,
    error: action.error
  }),
  [POST_UPDATE_PHONE_INIT]: (state, action) => ({
    ...state,
    postActivityPhoneDetails: undefined,
    error: undefined
  }),
  [POST_UPDATE_PHONE_SUCCESS]: (state, action) => ({
    ...state,
    postActivityPhoneDetails: action.response,
    error: undefined
  }),
  [POST_UPDATE_PHONE_FAILED]: (state, action) => ({
    ...state,
    postActivityPhoneDetails: undefined,
    error: action.error
  })
};

export default function helpTrackingReducer(state = initialState, action) {
  let handler = handlers[action.type];
  if (!handler) return state;
  return { ...state, ...handler(state, action) };
}