import {
    GET_IPADDRESS_INIT,
    GET_IPADDRESS_SUCCESS,
    GET_IPADDRESS_FAILED
} from '../actions/Actions';

export const initialState = {
    ipAddress: '',
    error: undefined
};

const handlers = {
    [GET_IPADDRESS_INIT]: (state, action) => ({
        ...state,
        ipAddress: undefined,
        error: undefined
    }),
    [GET_IPADDRESS_SUCCESS]: (state, action) => ({
        ...state,
        ipAddress: action.response.ip,
        error: undefined
    }),
    [GET_IPADDRESS_FAILED]: (state, action) => ({
        ...state,
        ipAddress: '',
        error: action.error
    })
}

export default function systemReducer(state = initialState, action) {
    let handler = handlers[action.type];
    if (!handler) return state;
    return { ...state, ...handler(state, action) };
}