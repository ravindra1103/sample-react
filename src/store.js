import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable'
import { routerReducer } from 'react-router-redux';
import { createEpicMiddleware } from 'redux-observable';

import system from './reducers/system';
import helptracking  from './reducers/helptracking';
import helpfeed from './reducers/helpfeed';
import { getHelpFeedSearchEpic } from './epics/helpfeed';


const rootEpic = combineEpics(
  getHelpFeedSearchEpic
);

const epicMiddleware = createEpicMiddleware(rootEpic);

const reducers = combineReducers({
  routing: routerReducer,
  system,
  helpfeed,
  helptracking
});


const initialState = {};
const enhancers = [];
const middleware = [
  thunk,
  epicMiddleware,
  routerMiddleware()
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  reducers,
  initialState,
  composedEnhancers
);

export default store;