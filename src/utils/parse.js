const parser = require('rss-parser');
const sanitizeHtml = require('sanitize-html');
const ApplicationConstants = require('./ApplicationConstants');

export function parseAndSortRssFeed(data, selectedMenu, searchTerm) {    
    let parsedData = [];
    let sortedResults = {};
    var options = {
        customFields: {
            item: ['category'],
        }
    }
    if (data) {
        parser.parseString(data.toString(), options, (err, parsed) => {
            parsedData = parsed;
        });
        if (parsedData && parsedData.feed && parsedData.feed.entries.length > 0) {
            let results = sortResults(parsedData.feed.entries, selectedMenu, searchTerm);
            sortedResults = results.data;
        }
        return { data: sortedResults, categories: (sortedResults) ? Object.keys(sortedResults) : [] };
    } else {
        return {};
    }
}

export function sortResults(helpEntries, selectedMenu, searchTerm) {
    let sortedElements = {};
    helpEntries.forEach(element => {
        if (typeof (element.category) === 'object') {
            element.category = element.category.$.term;
        }
        if (!element['content:encoded']) {
            element['content:encoded'] = element['content'];
        }
        if (element.id) {
            element.guid = element.id;
        }
        if (element.category) {
            if (canElementBeAdded(searchTerm, element)) {
                if (!sortedElements[categoryNameFilter(element.category)]) {
                    if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
                        if (element.category.toLowerCase().includes(ApplicationConstants.TUTORIAL_ACTIVITY_NAME)) {
                            sortedElements[categoryNameFilter(element.category)] = [];
                        }
                    } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
                        if (element.category.toLowerCase().includes(ApplicationConstants.FAQ_ACTIVITY_NAME)) {
                            sortedElements[categoryNameFilter(element.category)] = [];
                        }
                    }
                }
                if (selectedMenu === ApplicationConstants.TUTORIAL_MENU_ITEM_NAME) {
                    if (element.category.toLowerCase().includes(ApplicationConstants.TUTORIAL_ACTIVITY_NAME)) {
                        sortedElements[categoryNameFilter(element.category)].push({
                            value: sanitizeHtmlData(element['content:encoded']),
                            title: element.title.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">"),
                            guid: element.guid.split("?p=")[1],
                            updatedDate: element.pubDate,
                            feedbackReceived: false
                        });
                    }
                } else if (selectedMenu === ApplicationConstants.FAQS_MENU_ITEM_NAME) {
                    if (element.category.toLowerCase().includes(ApplicationConstants.FAQ_ACTIVITY_NAME)) {
                        sortedElements[categoryNameFilter(element.category)].push({
                            value: sanitizeHtmlData(element['content:encoded']),
                            title: element.title.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">"),
                            guid: element.guid.split("?p=")[1],
                            updatedDate: element.pubDate,
                            feedbackReceived: false
                        });
                    }
                }
            }
        }
    });
    return { data: getSortedObject(sortedElements) };
}

export function canElementBeAdded(searchTerm, element) {
    if (searchTerm &&
        (convertHTMLToPlainText(element['content:encoded']).toLowerCase().includes(searchTerm.toLowerCase())
            || convertHTMLToPlainText(element.title).toLowerCase().includes(searchTerm.toLowerCase()))) {
        return true;
    } else if (!searchTerm) {
        return true;
    }
    return false;
}

export function getSortedObject(object) {
    var sortedObject = {};
    var keys = Object.keys(object);
    keys.sort();

    for (let key of keys) {
        let value = object[key];
        sortedObject[key] = value;
    }
    return sortedObject;
}

export function categoryNameFilter(category) {
    return category.split('-').filter(element => !(element === 'Tutorial' || element === 'FAQ')).join('-');
}

export function parseResultsAfterFeedBack(helpFeed, guid, category) {
    let items = helpFeed[category];
    items = items.map(element => {
        if (element.guid === guid) {
            element.feedbackReceived = true;
        }
        return element;
    });
    helpFeed[category] = items;
    return { ...helpFeed };
}

export function sanitizeHtmlData(htmlDirty) {
    if (htmlDirty.includes('iframe')) {
        return htmlDirty;
    } else {
        return sanitizeHtml(htmlDirty, {
            allowedTags: ['h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
                'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
                'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre'],
            allowedAttributes: {
                a: ['href', 'name', 'target'],
                // We don't currently allow img itself by default, but this
                // would make sense if we did
                img: ['src']
            },
            // Lots of these won't come up by default because we don't allow them
            selfClosing: ['img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta'],
            // URL schemes we permit
            allowedSchemes: ['http', 'https', 'ftp', 'mailto'],
            allowedSchemesByTag: {},
            allowProtocolRelative: true,
            allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com']
        });
    }

}

export function searchItemsInCategory(helpFeed, category, searchTerm = '') {
    let searchResultsHelpFeed = {};    
    if (helpFeed && helpFeed[category]) {
        if (Array.isArray(helpFeed[category]) && helpFeed[category].length > 0) {
            let searchResults = helpFeed[category].filter(item => {
                if (convertHTMLToPlainText(item.value).toLowerCase().includes(searchTerm.toLowerCase())
                    || convertHTMLToPlainText(item.title).toLowerCase().includes(searchTerm.toLowerCase())) {
                    return true;
                }
                return false;
            });
            if (searchResults && searchResults.length > 0) {
                searchResultsHelpFeed[category] = searchResults;
            }
        }
    }
    return searchResultsHelpFeed;
}

export function searchItemsInHelpFeed(helpFeed, searchTerm = '') {
    let searchResultsHelpFeed = {};
    if (helpFeed) {
        for (let category in helpFeed) {
            if (Array.isArray(helpFeed[category]) && helpFeed[category].length > 0) {
                let searchResults = helpFeed[category].filter(item => {
                    if (convertHTMLToPlainText(item.title).toLowerCase().includes(searchTerm.toLowerCase())
                        || convertHTMLToPlainText(item.value).toLowerCase().includes(searchTerm.toLowerCase())) {
                        return true;
                    }
                    return false;
                });
                if (searchResults && searchResults.length > 0) {
                    searchResultsHelpFeed[category] = searchResults;
                }
            }
        }
    }
    return searchResultsHelpFeed;
}

export function convertCodetoSymbol(inputText) {
    inputText = encodeURIComponent(inputText)
    return decodeURIComponent(inputText).replace(/[\u2018\u2019]/g, "'")
        .replace(/[\u201C\u201D]/g, '"').replace(/\u00a0/g, " ")
        .replace(/&#8217;/g, "'").replace(/&#8220;/g, "'").replace(/&#8221;/g, '"');
}

export function convertHTMLToPlainText(inputText) {
    const tempElement = document.createElement("div");
    tempElement.innerHTML = convertCodetoSymbol(inputText);
    return tempElement.textContent || tempElement.innerText || "";
}

export function highlightSearch(text, search) {
    if (search && text) {
        search = search.toLowerCase();
        text = text.replace("’","'").replace("“","\"").replace("”","\"");
        let pattern = search.replace(/['<>-[\]/{}()*+?.\\^$|]/g, '\\$&');
        const regex = new RegExp(pattern + '(?!([^<]+)?>)', 'gi');
        if (text.toString().includes('<iframe'))
            return text;
        else {
            const tempElement = document.createElement("textarea");
            tempElement.innerHTML = text;
            text = tempElement.value;
            return text.replace(/<\/?highlight[^>]*>/g,"").replace(regex, (match) => `<highlight>${match}</highlight>`);
        }
    }
    else {
        return text;
    }
}