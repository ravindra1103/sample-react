const jwtDecode = require('jwt-decode');

const ApplicationConstants = require('./ApplicationConstants');

export function readTokenFromCookie() {
    let userToken = undefined;
    if (document.cookie) {
        let cookieArray = document.cookie.split('; ');
        for (let i = 0; i < cookieArray.length; i++) {
            let cookieVal = cookieArray[i].split('=');
            if (cookieVal[0] === ApplicationConstants.TOKEN_COOKIE_NAME) {
                userToken = cookieVal[1];
                break;
            }
        }
    }
    return userToken;
}

export function decodeToken(userToken) {
    let decodedValues = undefined;
    if(userToken) {
        decodedValues = jwtDecode(userToken);
    }
    return decodedValues;
}

export function isValidToken(userToken) {
    if(Math.floor((new Date()).getTime() / 1000)>jwtDecode(userToken).exp) 
        return false;
    else 
        return true;
}